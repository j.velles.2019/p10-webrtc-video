import argparse
import asyncio
import logging
import math
import json
import cv2
import numpy
from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling
from av import VideoFrame


class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1
        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frameCount = 0

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        global confirmedConnection, byeSignal
        confirmedConnection = data.decode()
        if confirmedConnection == "OK_Client":
            print("Connection established")
        if "bye" in confirmedConnection:
            byeSignal = True
            self.transport.close()
            quit(0)
    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)
        self.transport.close()


confirmedConnection = None
messageClient = None
echoConnection = None
byeSignal = None


async def waitRemoteDescription(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


async def waitForMessage():
    while confirmedConnection == "OK_Client":
        await asyncio.sleep(1)


async def waitForBye():
    while not byeSignal:
        await asyncio.sleep(1)


async def run(pc, player, recorder, signaling, role):
    global echoConnection, messageClient
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    messageClient = "REGISTER CLIENT"
    echoConnection = EchoClientProtocol(messageClient, on_con_lost)
    await loop.create_datagram_endpoint(lambda: echoConnection, remote_addr=('127.0.0.1', 9999))

    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

        async def framesCounting():
            nonlocal track
            while True:
                await track.recv()
                echoConnection.frameCount += 1
                print("Frame received:", echoConnection.frameCount)
                if echoConnection.frameCount == 150:
                    print(f"Limit number of frames reached -> {echoConnection.frameCount} \n Terminating connection...")
                    await waitForBye()
                    global messageClient
                    messageClient = {"type": "bye"}
                    byeMessage = json.dumps(messageClient)
                    echoConnection.transport.sendto(byeMessage.encode())
                    break
            await recorder.stop()
            await connectSignaling(pc, echoConnection)

        asyncio.ensure_future(framesCounting())

    async def connectSignaling(pc, echo):
        global confirmedConnection, messageClient
        # loop = asyncio.get_running_loop()
        # on_con_lost = loop.create_future()
        # messageClient = "REGISTER CLIENT"
        # if echo is None:
        #    echo = EchoClientProtocol(messageClient, on_con_lost)
        # await loop.create_datagram_endpoint(lambda: echo, remote_addr=('127.0.0.1', 9999))

        while True:
            await asyncio.sleep(3)
            if confirmedConnection == "OK_Client":
                obj = pc.localDescription
                messageClient = json.dumps(obj.__dict__)
                echo.transport.sendto(messageClient.encode())
                print("Send:", messageClient)
                await waitForMessage()

            if "sdp" and "answer" in confirmedConnection:
                await asyncio.sleep(2)
                answerDict = json.loads(confirmedConnection)
                answerSDP = answerDict['sdp']
                answerSDPType = answerDict['type']
                objAnswer = RTCSessionDescription(answerSDP, answerSDPType)
                if isinstance(objAnswer, RTCSessionDescription):
                    await pc.setRemoteDescription(objAnswer)
                    await waitRemoteDescription(pc)
                    await recorder.start()


    if role == "offer": # Creates an offer
        add_tracks()
        await pc.setLocalDescription(await pc.createOffer())

    await connectSignaling(pc, echoConnection)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("--role", default='offer')
    parser.add_argument("--play-from", help="Read the media from a file and sent it.", default="video.mp4"),
    parser.add_argument("--record-to", help="Write received media to a file."),
    parser.add_argument("--verbose", "-v", action="count")
    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # create signaling and peer connection
    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    # create media source
    if args.play_from:
        player = MediaPlayer(args.play_from)
    else:
        player = None

    # create media sink
    if args.record_to:
        recorder = MediaRecorder(args.record_to)
    else:
        recorder = MediaBlackhole()

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc=pc,
                player=player,
                recorder=recorder,
                signaling=signaling,
                role=args.role,
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())
