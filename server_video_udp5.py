import argparse
import asyncio
import logging
import math
import json
import ffmpeg
import cv2
import numpy
from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling
from av import VideoFrame

initialValue = 'value'
changeVar = initialValue    # variable que espera a ser asignada
sdpOffer = initialValue     # oferta a recibir del cliente
lastMessage = initialValue  # ultimo mensaje recibido

class EchoClientProtocol:
    def __init__(self, message, on_con_lost, name):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frame_count = 0    # contador de frames
        self.name = name        # Nombre del servidor

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        global lastMessage
        lastMessage = data.decode()
        if data.decode() == 'OK':
            print("Received:", data.decode())
        elif data.decode() != 'OK':
            print("Received:", data.decode())
            offer = json.loads(data.decode())  # Convierte string a diccionario
            sdp = offer['sdp']      # Almacena el sdp del mensaje
            tipo = offer['type']    # Almacena el tipo
            global sdpOffer
            sdpOffer = RTCSessionDescription(sdp, tipo)

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def wait_for_an_offer():      # Metodo que espera a recibir la ofera
    while sdpOffer == initialValue:
        await asyncio.sleep(10)
async def wait_for_remote_description(pc):      # Metodo que espera a establecer la conexion RTCP
    while pc.remoteDescription is None:
        await asyncio.sleep(1)




class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1
        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr


async def run(pc, player, recorder, signaling, role, name):
    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

        async def monitor_frames():     # Metodo para contar frames y parar la conexion
            nonlocal track          # 'nonlocal' permite modificar la variable que es local en run() para no crear otra
            while True:
                await track.recv()
                cliente.frame_count += 1
                if cliente.frame_count == (150):    # cuando llega al limite de frames
                    print(f"Limit number of bits reached [{cliente.frame_count}], closing connection...")
                    cliente.transport.sendto('{"type": "bye"}'.encode())    # parara la conexion
                    global lastMessage
                    lastMessage = '{"type": "bye"}'
                    break
            print('Closing channel')
            if recorder:
                await recorder.stop()
            await consumSignalling(pc, cliente)
        asyncio.ensure_future(monitor_frames())

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    registerMessage = f'REGISTER SERVER:{name}'

    cliente = EchoClientProtocol(registerMessage, on_con_lost, name)    # Establece conexion udp
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: cliente, remote_addr=('127.0.0.1', 6789))

    async def consumSignalling(pc, cliente):        # funcion para manejar el proceso de senyalizacion
            global sdpOffer, lastMessage
            if lastMessage == initialValue:     # no ha recibido ningun mensaje
                await wait_for_an_offer()
                await pc.setRemoteDescription(sdpOffer)     # estableciendo conexion remota
                await recorder.start()
                add_tracks()
                await pc.setLocalDescription(await pc.createAnswer())   # crea su respuesta
                sdpAnswerDict = pc.localDescription.__dict__
                sdpAnswer = json.dumps(sdpAnswerDict)
                print('Send: ', sdpAnswer)
                cliente.transport.sendto(sdpAnswer.encode())  # Send Answer to UDP server
                await wait_for_remote_description(pc)
            if lastMessage == '{"type": "bye"}':    # cuando se cierra el canal
                sdpOffer = initialValue             # restablecer : comtador, oferta, ultimo mensaje
                lastMessage = initialValue
                cliente.frame_count = 0
                pc = RTCPeerConnection()            # se crea nueva sesion rtcp
                print('Channel is closed with client')

                await run(
                    pc=pc,
                    player=player,
                    recorder=MediaRecorder(args.record_to),
                    signaling=signaling,
                    role=args.role,
                    name=args.name,
                )                                   # se ejecuta de nuevo run

            print()

    await consumSignalling(pc, cliente)
    try:
        await on_con_lost
    finally:
        transport.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("--role", default="answer")     # rol por defecto
    parser.add_argument("--play-from", help="Read the media from a file and sent it."),
    parser.add_argument("--record-to", help="Write received media to a file.", default="video-out.mp4"),
    parser.add_argument("--verbose", "-v", action="count")
    parser.add_argument("--name", default="HBO" )   # nombre del servidor por defecto

    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # create signaling and peer connection
    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    # create media source
    if args.play_from:
        player = MediaPlayer(args.play_from)
    else:
        player = None

    # create media sink
    if args.record_to:
        recorder = MediaRecorder(args.record_to)

    else:
        recorder = MediaBlackhole()

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc= pc,
                player=player,
                recorder=recorder,
                signaling=signaling,
                role=args.role,
                name=args.name,
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())



