import asyncio

directionClient = None
directionServer = None


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        global directionServer, directionClient
        if message == "REGISTER SERVER" or message == "REGISTER CLIENT":
            if message == "REGISTER SERVER":
                directionServer = addr
                print('Received %r from %s' % (message, directionServer))
                print('Registering server, sending OK')
                answerForRegisterServer = "OK_Server"
                print('Send %r to %s' % (answerForRegisterServer, addr))
                self.transport.sendto(answerForRegisterServer.encode(), directionServer)
            if message == "REGISTER CLIENT":
                directionClient = addr
                print('Received %r from %s' % (message, addr))
                print('Registering client, sending OK')
                answerForRegisterClient = "OK_Client"
                print('Send %r to %s' % (answerForRegisterClient, directionClient))
                self.transport.sendto(answerForRegisterClient.encode(), directionClient)
        else:
            if "sdp" and "offer" in message:
                print("Client offer")
                print('Received %r from %s' % (message, addr), "CLIENT")
                directionClient = addr
                print("Client Direction -> ", directionClient)
                self.transport.sendto(data, directionServer)
                print('Sending %r to %s' % (data, directionServer))
            if "sdp" and "answer" in message:
                print("Server answer")
                print('Received %r from %s' % (message, addr))
                directionServer = addr
                print("Server Direction -> ", directionServer)
                self.transport.sendto(data, directionClient)
                print('Sending %r to %s' % (data, directionClient))


async def main():
    print("Starting UDP server")
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


if __name__ == "__main__":
    asyncio.run(main())
