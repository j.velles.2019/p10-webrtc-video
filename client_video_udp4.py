import argparse
import asyncio
import logging
import math
import json

import cv2
import numpy
from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling
from av import VideoFrame

initialValue = 'value'      # valor inicial de las variables globales
desc = initialValue         # Almacenara la descripcion local del sdp
sdpAnswer = initialValue    # Almacenara la descripcion remota
recvBye = False             # Booleano que indica si se recibio el comando bye

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frame_count = 0  # Agrega un contador de frames


    def connection_made(self, transport):
        self.transport = transport
        registerMessage= 'REGISTER CLIENT'
        print('Send:', registerMessage)
        self.transport.sendto(registerMessage.encode())


    def datagram_received(self, data, addr):

        if data.decode() == 'OK':
            print("Received:", data.decode())
            global desc
            self.transport.sendto(desc.encode())
            print('Send:', desc)

        elif data.decode() != 'OK':
            print("Received:", data.decode())
            if data.decode() == '{"type": "bye"}':
                global recvBye
                recvBye = True
                print('Exiting...')
            else:
                answer = json.loads(data.decode())      # extrayendo answer como diccionario
                sdp = answer['sdp']
                tipo = answer['type']
                global sdpAnswer
                sdpAnswer = RTCSessionDescription(sdp, tipo)    # Instanciando la descripcion remota como RTC

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

# Funcion que espera a recibir el mensaje con la descripcion remota
async def wait_for_an_answer():
    global sdpAnswer
    while sdpAnswer == initialValue:
        await asyncio.sleep(1)

# Funcion que espera a recibir por el comando bye
async def wait_for_bye():
    global recvBye
    while recvBye == False:
        await asyncio.sleep(1)

# Funcion que espera por la descripcion remota
async def wait_for_remote_description(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1
        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr


async def run(pc, player, recorder, signaling):
    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

        # Funcion que controla el envio de los frames y cierra la conexión
        async def monitor_frames():
            nonlocal track
            while True:
                await track.recv()
                cliente.frame_count += 1
                if cliente.frame_count == (150):
                    print(f"Limit number of bits reached [{cliente.frame_count}], closing connection...")
                    await wait_for_bye()
                    break
            print('Closing Channel...')
            await recorder.stop()
            await pc.close()
            try:
                cliente.transport.close()       # Al cerrar la conexión se cierra el cliente udp
            except:
                print("Error closing connection")
                quit()
        asyncio.ensure_future(monitor_frames())

    # wait for signaling
    # send offer
    add_tracks()
    await pc.setLocalDescription(await pc.createOffer())    # Se crea la oferta
    offer_dict = pc.localDescription.__dict__
    offer_str = json.dumps(offer_dict)
    global desc
    desc = offer_str

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    cliente = EchoClientProtocol(desc, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: cliente, remote_addr=('127.0.0.1', 6789))

    while True:
        await wait_for_an_answer()
        await pc.setRemoteDescription(sdpAnswer)
        await wait_for_remote_description(pc)
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("--role", default="offer" )         # Sera siempre 'offer'
    parser.add_argument("--play-from", help="Read the media from a file and sent it.", default='video.mp4'),
    parser.add_argument("--record-to", help="Write received media to a file."),
    parser.add_argument("--verbose", "-v", action="count")
    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # create signaling and peer connection
    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    # create media source
    if args.play_from:
        player = MediaPlayer(args.play_from)
    else:
        player = None

    # create media sink
    if args.record_to:
        recorder = MediaRecorder(args.record_to)
    else:
        recorder = MediaBlackhole()

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc=pc,
                player=player,
                recorder=recorder,
                signaling=signaling,
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())
